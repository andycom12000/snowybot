from sqlalchemy import create_engine, Integer, String, DateTime, Boolean, Column, select
from sqlalchemy.orm import sessionmaker, declarative_base
from secrets import *

Session = sessionmaker()
Base = declarative_base()

db_credential = 'mysql+pymysql://%s:%s@%s/%s' % (MYSQL_USERNAME, MYSQL_PASSWORD,
                                                 MYSQL_HOSTNAME, MYSQL_DATABASE)
db = create_engine(db_credential, pool_recycle=3600, pool_pre_ping=True)
Session.configure(bind=db)

class CommandString(Base):
    __tablename__ = 'strings'
    string_id = Column(Integer, primary_key=True, autoincrement=True)
    command_name = Column(String)
    content = Column(String)

    def __repr__(self):
        return '<CommandString #%s>' % self.string_id


def _get_random_string_from_database(command_name):
    session = Session()
    stmt = select(CommandString).where(CommandString.command_name == command_name)
    result = session.execute(stmt)
    for obj in result.scalars():
        print(obj)
