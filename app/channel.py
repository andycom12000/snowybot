import requests

class Channel:
    def __init__(self, channel_name, client_id, token):
        self.channel_name = channel_name
        self.client_id = client_id
        self.token = token
        #self.channel_id = self._get_channel_id()

    def _get_channel_id(self):
        print('Getting channel id: ' + self.channel_name)
        url = 'https://api.twitch.tv/helix/users?login=' + self.channel_name
        headers = {'Client-ID': self.client_id, 'Accept': 'application/vnd.twitchtv.v5+json', 'Authorization': 'Bearer fedklop75zqamqywm075j7yuy1d3xs'}
        r = requests.get(url, headers=headers).json()
        return r['data'][0]['id']

    def __repr__(self):
        return '<Channel #%s>' % self.channel_name

    def get_channel_name(self):
        return '#' + self.channel_name

    def get_current_viewers(self):
        url = 'https://tmi.twitch.tv/group/user/' + self.channel_name + '/chatters'

        try:
            r = requests.get(url, timeout=1).json()
        except:
            return None
        return r['chatter_count']
