import threading
import time
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.modules.twitchbot import TwitchBot
from app.models import Session
from app.models.channel import Channel
from secrets import *


def thread_job(bot):
    bot.start()

class SnowyBot:
    def __init__(self):
        db_credential = 'mysql+pymysql://%s:%s@%s/%s' % (MYSQL_USERNAME, MYSQL_PASSWORD,
                                                         MYSQL_HOSTNAME, MYSQL_DATABASE)
        self.db = create_engine(db_credential, pool_recycle=3600,
                                pool_pre_ping=True, pool_size=10)
        Session.configure(bind=self.db)
        self.channels = self._get_channels_from_database()
        self.bots = self._create_bots_by_channels()
        self.threads = self._create_threads_by_bots()

    def _get_channels_from_database(self):
        with Session() as session:
            channel_names = [name for name, in session.query(Channel.channel_name).filter_by(active=True).all()]
        return channel_names

    def _create_bots_by_channels(self):
        total_bot_count = int(len(self.channels) / 20) + 1
        bot_list = list()
        for i in range(total_bot_count):
            target_channels = self.channels[i*20:i*20+20]
            bot_list.append(TwitchBot(USERNAME, CLIENT_ID, TOKEN, target_channels))
            time.sleep(5)
        return bot_list

    def _create_threads_by_bots(self):
        thread_list = list()
        for bot in self.bots:
            thread_list.append(threading.Thread(target=thread_job, args=(bot,)))
        return thread_list

    def start(self):
        for thread in self.threads:
            print('Starting thread - ', thread)
            thread.start()
            print('Waiting until next join period')
            time.sleep(15)
        self._wait_for_threads()

    def _wait_for_threads(self):
        for thread in self.threads:
            thread.join()
            print('Joined thread - ', thread)
