from sqlalchemy import Integer, String, DateTime, Boolean, Column
from app.models import Base

class CommandString(Base):
    __tablename__ = 'strings'
    string_id = Column(Integer, primary_key=True, autoincrement=True)
    command_name = Column(String)
    content = Column(String)

    def __repr__(self):
        return '<CommandString #%s>' % self.string_id
