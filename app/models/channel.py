from sqlalchemy import Integer, String, DateTime, Boolean, Column
from app.models import Base

class Channel(Base):
    __tablename__ = 'channels'
    id = Column(Integer, primary_key=True, autoincrement=True)
    channel_name = Column(String)
    active = Column(Boolean)
    registered_time = Column(DateTime)

    def __repr__(self):
        return '<Channel #%s>' % self.channel_name
