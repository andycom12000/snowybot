from sqlalchemy.orm import declarative_base, sessionmaker

Session = sessionmaker()
Base = declarative_base()
