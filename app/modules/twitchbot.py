import sys
import time
import irc.bot
import requests
import random
import datetime
from sqlalchemy import select, func
from app.channel import Channel
from app.models import Session
from app.models.strings import CommandString


class TwitchBot(irc.bot.SingleServerIRCBot):
    def __init__(self, username, client_id, token, channels):
        self.client_id = client_id
        self.token = token
        self.snowy_channels = [Channel(channel, client_id, token) for channel in channels]

        self.commands = {
            '俄羅斯輪盤': self.russian_roulette,
            '自殺': self.suicide,
            '指令': self.show_command_list,
            '吃': self.random_food,
            '抽': self.draw_card,
            '抽鬼': self.draw_killer,
            '守靈': self.get_current_viewers,
            '喝': self.random_drink,
            '猜拳': self.morra,
            '賭盤': self.draw_prediction,
            '翻譯蒟蒻': self.random_translate,
            '看': self.draw_anime_or_movie,
            '打架': self.fight_with_bot,
            'so': self.shout_out,
            'eric': self.make_fun_of_eric
        }

        # Create IRC bot connection
        server = 'irc.chat.twitch.tv'
        port = 6667
        print('Connecting to ' + server + ' on port ' + str(port) + '...')
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port, 'oauth:'+token)], username, username)
        time.sleep(1)

    def on_welcome(self, c, e):
        # You must request specific capabilities before you can use them
        for channel in self.snowy_channels:
            print('Joining channel ' + channel.get_channel_name())
            c.cap('REQ', ':twitch.tv/membership')
            c.cap('REQ', ':twitch.tv/tags')
            c.cap('REQ', ':twitch.tv/commands')
            c.join(channel.get_channel_name())

    def on_pubmsg(self, c, e):
        # If a chat message starts with an exclamation mark, try to run it as a command
        current_time = str(datetime.datetime.now())
        if e.arguments[0][:1] == '!':
            cmd = e.arguments[0].split(' ')[0][1:].lower()
            print('[' + current_time + '] Received command: ' + cmd + ' from ' + e.target)
            self.do_command(e, cmd)

    def do_command(self, e, cmd):
        self.commands.get(cmd, self.default_command)(e)

    def _get_username_from_event(self, e):
        return e.source.split('!')[0]

    def _get_display_name_from_event(self, e):
        for tag in e.tags:
            if tag['key'] == 'display-name':
                return tag['value']
        return ''

    def russian_roulette(self, e):
        c = self.connection

        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)

        rand_param = random.randint(0, 100)
        big_ban_chance = 5
        big_ban_duration = 600
        small_ban_chance = 10
        small_ban_duration = 60

        if rand_param >= (100 - big_ban_chance):
            message = display_name + '中彈了！ Σ( ° △ °'
            c.privmsg(e.target, message)
            message = '/timeout ' + username + ' ' + str(big_ban_duration)
            c.privmsg(e.target, message)
        elif rand_param >= (100 - big_ban_chance - small_ban_chance):
            message = display_name + '被子彈擦到了！ Σ( ° △ °'
            c.privmsg(e.target, message)
            message = '/timeout ' + username + ' ' + str(small_ban_duration)
            c.privmsg(e.target, message)
        else:
            messages = [
                ' 過關沒事',
                ' 閃過一槍',
                ' 躲避成功',
                ' 差點掛了',
                ' 真是好險',
                ' 差點沒命',
                ' 中...哈哈沒中啦'
            ]
            emotes = [
                ' (`･ω･)ゞ',
                ' ⁀⊙﹏☉⁀',
                ' (*｀･ω･)ゞ',
                ' ＼(^O^ )＞*',
                ' o(*￣○￣)ゝ',
                ' ヽ|･ω･|ゞ',
                ' (ﾟω。)'
            ]
            message = display_name + messages[random.randint(0, len(messages)-1)] + emotes[random.randint(0, len(messages)-1)]
            message = '/me ' + message
            c.privmsg(e.target, message)

    def suicide(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        print('Killing ' + username)
        message = '/timeout ' + username + ' 1 又做了什麼虧心事齁'
        c.privmsg(e.target, message)

    def show_command_list(self, e):
        c = self.connection
        print('Showing commands')
        message = '/me 目前指令有: '
        for command in self.commands.keys():
            message += '!' + command + ' '
        c.privmsg(e.target, message)

    def random_food(self, e):
        c = self.connection
        food = self._get_random_string_from_database('food')
        message = '吃 ' + food + ' ！'
        c.privmsg(e.target, message)

    def draw_card(self, e):
        c = self.connection
        display_name = self._get_display_name_from_event(e)
        rand_param = random.randint(0, 100)

        if rand_param < 5:
            message = display_name + ' 抽中了一星垃圾，尼怎麼可以這麼爛 WutFace'
        elif rand_param < 25:
            message = display_name + ' 抽中了二星N卡，丟水溝撲通 MrDestructoid'
        elif rand_param < 75:
            message = display_name + ' 抽中了三星R卡，普普通通還行 SabaPing'
        elif rand_param < 95:
            message = display_name + ' 抽中了四星SR卡，太神啦 PogChamp'
        else:
            message = 'MercyWing1 ' + display_name + ' 抽中了五星SSR卡，我的老天你是鬼吧 MercyWing2'
        message = '/me ' + message
        c.privmsg(e.target, message)

    def draw_killer(self, e):
        c = self.connection
        killer = self._get_random_string_from_database('killer')
        message = '/me 請使用你最強的「' + killer + '」達到無情殺手吧！'
        c.privmsg(e.target, message)

    def get_current_viewers(self, e):
        c = self.connection
        channel = self._get_channel_by_target(e.target)
        current_viewers = channel.get_current_viewers()
        if current_viewers:
            message = '現在有 ' + str(current_viewers) + ' 人在守靈 \|/'
        else:
            message = '伺服器忙碌中，稍後再試'
        c.privmsg(e.target, message)

    def _get_channel_by_target(self, target):
        name = target[1:]
        for channel in self.snowy_channels:
            if name == channel.channel_name:
                return channel
        return None

    def random_drink(self, e):
        c = self.connection
        drink = self._get_random_string_from_database('drink')
        message = '喝 ' + drink + ' ！'
        c.privmsg(e.target, message)

    def morra(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)

        morra_choices = ['剪刀', '石頭', '布']

        params = e.arguments[0].split(' ')
        if len(params) > 1:
            question = params[1]
        else:
            message = display_name + ' 要猜拳就給我好好猜 SMOrc'
            message = '/me ' + message
            c.privmsg(e.target, message)
            return

        answer = random.choice(morra_choices)
        if question not in morra_choices:
            message = display_name + ' 要猜拳就給我好好猜 SMOrc'
            message = '/me ' + message
            c.privmsg(e.target, message)
            return

        win_result = ''
        result_message = {'lose': '輸了', 'win': '贏了', 'draw': '平手'}
        if question == '剪刀' and answer == '石頭':
            win_result = result_message['lose']
        elif question == '剪刀' and answer == '布':
            win_result = result_message['win']
        elif question == '石頭' and answer == '布':
            win_result = result_message['lose']
        elif question == '石頭' and answer == '剪刀':
            win_result = result_message['win']
        elif question == '布' and answer == '剪刀':
            win_result = result_message['lose']
        elif question == '布' and answer == '石頭':
            win_result = result_message['win']
        else:
            win_result = result_message['draw']

        message = display_name + '出' + question + '， MrDestructoid 出' + answer + '，你' + win_result
        message = '/me ' + message
        c.privmsg(e.target, message)

    def draw_prediction(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)
        draw_choices = ['就決定是藍藍幫了！', '粉紅幫歐印上吧！', '你褲子都沒了還賭！']
        message = random.choice(draw_choices)
        message = '/me ' + display_name + ' ' + message
        c.privmsg(e.target, message)

    def random_translate(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)
        params = e.arguments[0].split(' ')
        if len(params) > 1:
            original_question = params[1]
        else:
            message = display_name + ' 後面給我加上要翻譯的東西 SMOrc'
            message = '/me ' + message
            c.privmsg(e.target, message)
            return

        draw_choices = ['好耶，今天開到死', '還是這邊先生氣？', '每過六十秒就有一分鐘過去',
                        '人被殺就會死', '早餐單傳到哪裡了？', '啊我就怕被罵啊！', '看主播這樣開，你很勇喔？',
                        '當時...就...挺突然的...', '那天，我拎著一把刀，揮下去，他就死掉了..',
                        '然後呢？然後他就死掉了', '我也是真的很想上車', '這是一把有毒的刀(舔',
                        '只要每天省下買一杯奶茶的錢，十天後就能買十杯奶茶', '一週不見，如隔七日',
                        '拜託slowly送，送慢點好不好', '炸物跟冷飲是不好的 如果戒不掉，這裡有賣',
                        '我D了欸', '啊你為什麼不提早D？', 'wwwww(燦笑(摸頭',
                        '傑哥不要', '讓我看看！', '我看你是完全不懂喔', '歡迎你們來我家玩，玩累了就直接睡覺，沒問題的。',
                        '恰恰(捏橘子', '一般的狗、貓、馬、天竺鼠、勞贖？', ' 你要不要吃哈密哈？',
                        '我好了', ' 我越來越接近電話了', '我都念IKEA', '我都念COSTCO',
                        '不要再打了，要打去練舞室打！', '不要瞎掰好嗎', '初四啦！！阿伯！',
                        'YO！Battle', '你要不要聽聽看你現在在講什麼', '八嘎NONO 馬西馬西',
                        '我們的半糖是正常喔', ' 我的豆花！！三十塊！！', '阿罵你怎麼沒感覺',
                        'Play蝦米Game', '我一直都很專情', '奇怪的知識增加了',
                        '拿啦拿啦拿啦，拿一下啦（漁夫調', '好耶', '阿扁巴巴大俠',
                        '甘安捏', '不錄了啦！', '再再再一場', '下次一定',
                        '小朋友才看鬼滅，大人都看天竺鼠車', '小朋友才做選擇，我全都要',
                        '又舔...又舔...又舔嘴唇', '前面有一隻超可愛的狗勾，狗～勾～',
                        'https://youtu.be/dQw4w9WgXcQ',
                        '勤能補拙，但不能補醜']
        hashed_index = hash(original_question) % len(draw_choices)
        message = draw_choices[hashed_index]
        message = '/me ' + display_name + ' 剛剛說了：「' + message + '」'
        c.privmsg(e.target, message)

    def draw_anime_or_movie(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)
        draw_choices = ['前輩有夠煩', '萬能戰士無比敵', '世界盡頭的騎士', '半妖的夜叉姬',
                        '從零開始的魔法書', '入間同學入魔了', 'Fate/Stay night [Heaven\'s Feel]', '北極熊咖啡廳',
                        '路人超能100', '庫洛魔法使', '辣妹與恐龍', '天能', ' 奇巧計程車',
                        '博多豚骨拉麵團', 'Beastars', '別對映像研出手', '動物新世代 BNA',
                        '衝吧烈子', '怕痛的我把防禦力點滿就對了', '空之境界',
                        '末日時在做什麼？有沒有空？可以來拯救嗎？', '遊戲三人娘',
                        '女高中生的虛度日常', '無職轉生，到了異世界就拿出真本事',
                        '冰菓', '小林家的龍女僕', '未聞花名', '齊木楠雄的災難',
                        'Jojo的奇幻冒險', '鋼之鍊金術師', '魔法少女小圓', '怪獸與牠們的產地',
                        '龍王的工作', '怪奇物語', '絕命毒師', '魷魚遊戲', '荒唐分局',
                        '冰與火之歌', '愛死機器人', '謀殺入門課', '后翼棄兵', '紙牌屋',
                        'Sense 8', '今際之國的闖關者', '紀錄的地平線', '境界線上的地平線', 'Rick and Morty',
                        '在地下城尋求邂逅是否搞錯了什麼', '狂賭之淵', '涼宮春日的消失',
                        '紫羅蘭永恆花園', '史蒂芬宇宙', '我想吃掉你的胰臟', '關於我轉生變成史萊姆這檔事',
                        '86－不存在的戰區', 'Vivy -Fluorite Eye\'s Song-', '刃牙',
                        '炎炎消防隊', '良善之地', '降世神通', '奇諾之旅', '哈利波特',
                        '神秘小鎮大冒險', '太空終界', '異獸魔都', '魔獸世界', '爆肝工程師的異世界狂想曲',
                        '阿甘妙世界', '馬男波傑克', '帶著智慧型手機闖蕩異世界', '性愛自修室',
                        'Adventure Time', '人生差一點', '勇者義彥', '暗殺教室', '崩壞夢王國',
                        '歡迎光臨宇宙秀', '新石紀', '新世紀福音戰士', '可愛巧虎島',
                        '天線寶寶', 'Another', 'School Days', '波西傑克森', '極道主夫'
                        '返校', '史瑞克', '除了黑白和旅途的Pokemon', '獵人', '工作細胞'
                        '轉生成蜘蛛又怎樣', '我的英雄學院', '這個勇者明明超TUEEE卻過度謹慎',
                        '遊戲王', '動物朋友第一季', '妖精的尾巴', '通靈之戰', '咒術迴戰',
                        '飛哥與小佛', '火影忍者', '犬夜叉', '約定的夢幻島', '來自深淵',
                        '黃金神威', '海盜戰記', '南方四賤客', '刀劍神域', '海綿寶寶',
                        '馴龍高手', '十月圍城', '你的孩子不是你的孩子', '楚門的世界',
                        '三個傻瓜', '神偷奶爸', '黑鏡', 'Steins;Gate', '馬達加斯加',
                        '遊戲人生', '寵物當家', '哆啦A夢', '大嘴鳥']

        suggestion_message_list = ['這邊誠心推薦 %s 一定要去看',
                                   '這部太神了 %s 還不去看一下',
                                   '%s 要不要看看',
                                   '欸 %s 前面有部超棒的']

        suggestion_message = random.choice(suggestion_message_list)
        message = random.choice(draw_choices)
        message = '/me ' + (suggestion_message%display_name) + ' 「' + message + '」'
        c.privmsg(e.target, message)

    def fight_with_bot(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)

        rand_param = random.randint(0, 100)
        big_ban_chance = 10
        big_ban_duration = 60
        small_ban_chance = 20
        small_ban_duration = 30

        if rand_param >= (100 - big_ban_chance):
            message = display_name + 'lswoofFight 雪寶把你揍的鼻青臉腫！'
            c.privmsg(e.target, message)
            message = '/timeout ' + username + ' ' + str(big_ban_duration)
            c.privmsg(e.target, message)
        elif rand_param >= (100 - big_ban_chance - small_ban_chance):
            message = display_name + '你跟雪寶打成平手 FBtouchdown '
            c.privmsg(e.target, message)
            message = '/timeout ' + username + ' ' + str(small_ban_duration)
            c.privmsg(e.target, message)
        else:
            message = '/me ' + display_name + ' 雪寶完全不是你的對手！ lswoofDead'
            c.privmsg(e.target, message)

    def shout_out(self, e):
        c = self.connection
        username = self._get_username_from_event(e)
        display_name = self._get_display_name_from_event(e)

        identity_tag = e.tags[1]['value']
        if identity_tag is None or ('broadcaster' not in identity_tag and 'moderator' not in identity_tag):
            return

        params = e.arguments[0].split(' ')
        print(params)
        if len(params) == 2:
            twitch_id = params[1]
            if twitch_id and twitch_id[0] == '@':
                twitch_id = twitch_id[1:]
        else:
            message = display_name + ' !so [TwitchID] 請在指令後加入目標的TwitchID'
            message = '/me ' + message
            c.privmsg(e.target, message)
            return

        message = '/me 大家可以去追隨 https://twitch.tv/' + twitch_id + ' ，他也是個很棒的主播'
        c.privmsg(e.target, message)

    def make_fun_of_eric(self, e):
        c = self.connection
        message = self._get_random_string_from_database('eric')
        c.privmsg(e.target, message)

    def _get_random_string_from_database(self, command_name):
        with Session() as session:
            stmt = select(CommandString) \
                  .where(CommandString.command_name == command_name) \
                  .order_by(func.rand()) \
                  .limit(1)
            result = session.execute(stmt).scalar()
        return result.content

    def default_command(self, e):
        pass
