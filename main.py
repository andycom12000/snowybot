from app import SnowyBot

app = SnowyBot()

if __name__ == '__main__':
    print('Snowy Bot is now running...')
    app.start()
